
var assert = require('assert');
var user=require('../test/user');

describe('数据库测试',()=>{

    before('创建数据库表',()=>{
        console.log('测试前。。。');
        
    });

    it('查看返回数据',()=>{
        user.dropTable();
        assert.strictEqual(user.createTable());
    });

    it('list data',function() {
        assert.strictEqual(user.query());
    })
    after('清空数据库',()=>{
        console.log('测试后。。。');
        assert.strictEqual(user.dropTable());
    });

});