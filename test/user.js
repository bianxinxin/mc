// 导入MySQL模块
var mysql = require('mysql');
var dbConfig = require('../DB/dbConf');
var userSQL = require('../DB/usersql');
// 使用DBConfig.js的配置信息创建一个MySQL连接池
var pool = mysql.createPool( dbConfig.mysql );

function createTable(){
    pool.getConnection((error,connection)=>{
      connection.query(userSQL.createTable,(err,result)=>{
        if(err){
          console.log('fail'+err);
        }
      });
      connection.query(userSQL.insert,(err,result)=>{
        if(err){
          console.log('fail insert!'+err);
        }
      });
      connection.release();
    });
}
function query(){
  pool.getConnection(function(error,connection){
    connection.query(userSQL.queryAll,function(err,result){
      if(err) throw err;
    });
    connection.release();
  });
}

function dropTable() {
  pool.getConnection(userSQL.dropTable,(err,connection)=>{
    if(err) throw err;
  });
}

exports.createTable=createTable;
exports.query=query;
exports.dropTable=dropTable;