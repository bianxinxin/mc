var assert = require('assert');
var user=require('../User/User');

describe('用户操作测试',()=>{

    it('用户正确登录测试',()=>{
        assert.strictEqual(user.userLogin('李四','lisi'));
        assert.strictEqual(user.userLogin('张三','admin'));
        assert.strictEqual(user.userLogin('张三','admin1'));
    });
});