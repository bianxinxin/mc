//user操作语句
var UserSQL = {
    createTable:'CREATE TABLE IF NOT EXISTS user(uid VARCHAR(40) NOT NULL PRIMARY KEY,username VARCHAR(50),password VARCHAR(50),status tinyint)ENGINE=INNODB DEFAULT CHARSET="utf8"',
    insert:'INSERT INTO user(uid,uname,password) VALUES(?,?,?)', 
    queryAll:'SELECT * FROM user',  
    getUserById:'SELECT * FROM user WHERE uid = ? ',
    dropTable:'DROP TABLE `user`',
    loginCheck:'SELECT status FROM user WHERE uname=? and password=?'
  };
module.exports = UserSQL;