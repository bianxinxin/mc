var express = require('express');
var router = express.Router();
// 导入MySQL模块
var mysql = require('mysql');
var dbConfig = require('../DB/dbConf');
var userSQL = require('../DB/usersql');
// 使用DBConfig.js的配置信息创建一个MySQL连接池
var pool = mysql.createPool( dbConfig);
//导入uuid
var uuid=require('node-uuid');
/////////////////////////////用户相关///////////////////////////////////////////////////////
//用户登录
router.post('/logincheck',function(req,res,next){
  pool.getConnection((err,connection)=>{
    connection.query(userSQL.loginCheck,[req.body.username,req.body.password],(err,resutls)=>{
      if (err) throw err;
      if(resutls.length>0){
        if(resutls[0].status==1){
          //存入session
          req.session.username=req.body.username;
        }
        res.json(resutls[0].status);
      }else{
        res.json('-1');
      }
      connection.release();
    });
  });
});

//获取用户信息
router.get('/getUserInfo',function(req,res,next) {
  pool.getConnection((err,connection)=>{
    connection.query(userSQL.getUserById,req.query.uid,(err,resutls)=>{
        if (err) throw err;
        if(resutls.length>0){
          res.end(JSON.stringify(resutls));
        }else res.end('null');
    });
    connection.release();
  }); 
});
/////////////////////////////用户微课程相关///////////////////////////////////////////////////////
//根据知识点id查找知识点
router.get('/getKnowlegeById',(req,res,next)=>{
  var konwlegeid= req.query.konwlegeid;
  pool.getConnection((err,connection)=>{
    if(err) throw err;
    var sql='SELECT k.`name`,k.content,k.videopic,k.videosrc,k.images,k.visitcount,c.`name` AS catename,o.`name` AS originname FROM knowledge AS k LEFT JOIN origin as o ON(k.origin_id=o.uuid) LEFT JOIN cate AS c ON(k.cate_id=c.id) WHERE k.`status`=1 AND k.uuid=?';
    connection.query(sql,konwlegeid,(err,resutls)=>{
      if(err) throw err;
      res.json(resutls);
    });
    connection.release();
  });
});

//关键字搜索列表
router.get('/getKnowlegeByKey',(req,res,next)=>{
  var key=req.query.key;
  if(!key) return res.json({result:null});
  pool.getConnection(function(err,connection){
    if(err){
        throw err;
    }
    var sql='SELECT uuid,`name` FROM knowledge WHERE `name` LIKE "%'+key+'%" limit 3';
    connection.query(sql,function(err,resutls){
      if(err){throw err}
      res.json(resutls);
    });
    connection.release();
  });
});
// 随机推荐课程
router.get('/randomCourses',(req,res,next)=>{
  pool.getConnection(function(err,connection){
    if(err)throw err;
    var sql='SELECT k.`name`,k.videopic,k.visitcount,c.`name` AS catename,o.`name` AS originname FROM knowledge AS k LEFT JOIN origin as o ON(k.origin_id=o.uuid) LEFT JOIN cate AS c ON(k.cate_id=c.id) ORDER BY RAND() LIMIT 12';
    connection.query(sql,function(err,resutls){
      if(err)throw err
      res.json(resutls);
    });
    connection.release();
  });
});
// 根据专业id显示课程
router.get('/listCoursesbyid',(req,res,next)=>{
  pool.getConnection(function(err,connection){
    if(err)throw err;
    var sql='SELECT k.`name`,k.videopic,k.visitcount,c.`name` AS catename,o.`name` AS originname FROM knowledge AS k LEFT JOIN origin as o ON(k.origin_id=o.uuid) LEFT JOIN cate AS c ON(k.cate_id=c.id) WHERE cate_id=?';
    connection.query(sql,req.query.pid,function(err,resutls){
      if(err)throw err
      res.json(resutls);
    });
    connection.release();
  });
});
module.exports = router;
