var express = require('express');
var router = express.Router();
// 导入MySQL模块
var mysql = require('mysql');
var dbConfig = require('../DB/dbConf');
var userSQL = require('../DB/usersql');
// 使用DBConfig.js的配置信息创建一个MySQL连接池
var pool = mysql.createPool( dbConfig);
//导入uuid
var uuid=require('node-uuid');

//导入文件上传中间件   multer配置
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
var upload = multer({ storage: storage });

//用户登录
router.post('/logincheck',function(req,res,next){
  pool.getConnection((err,connection)=>{
    console.log(req.ip);
    console.log(req.body);
    connection.query(userSQL.loginCheck,[req.body.username,req.body.password],(err,resutls)=>{
        if (err) throw err;
        if(resutls.length>0){
          //存入session
          req.session.uname=req.body.username;
          res.send('1');
        }else{
          res.send('0');
        }
        connection.release();
    });
  });
});

//用户注册
router.post('/userreg',function(req,res,next){
  pool.getConnection((err,connection)=>{

    connection.query(userSQL.insert,[uuid.v1(),req.body.uname,req.body.password],(err,resutls)=>{
        if (err) throw err;
        if(resutls.affectedRows==1) res.send('注册成功');
        else res.send('注册失败!');
    });
    connection.release();
  });
});
//获取用户信息
router.get('/getUserInfo',function(req,res,next) {
  pool.getConnection((err,connection)=>{
    connection.query(userSQL.getUserById,req.query.uid,(err,resutls)=>{
        if (err) throw err;
        if(resutls.length>0) res.end(JSON.stringify(resutls));
        else res.end('null');
    });
    connection.release();
  }); 
});

//资料的添加功能(图片，文字，视频)
router.post('/upload',upload.any(),function(req,res,next){

  console.log(req.body);
  console.log(req.files);
  return;
  pool.getConnection((err,connection)=>{
    if(err) throw err;
    //视频上传

    //图片上传

    //文字上传

    //写入数据库

    connection.query('','',(err,results)=>{
      if(err) throw err;

    });
    connection.release();
  });
});

module.exports = router;
