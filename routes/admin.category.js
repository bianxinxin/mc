var express = require('express');
var router = express.Router();
// 导入MySQL模块
var mysql = require('mysql');
var dbConfig = require('../DB/dbConf');
var cateSQL = require('../DB/catesql');
// 使用DBConfig.js的配置信息创建一个MySQL连接池
var pool = mysql.createPool( dbConfig);
//导入uuid
var uuid=require('node-uuid');

// 增加分类
router.get('/addcategory',function(req,res,next){
  pool.getConnection((err,connection)=>{
    var sql='INSERT INTO category(cname,ccomment) values(?,?)';
    connection.query(sql,[req.query.cname,req.query.ccomment],(err,resutls)=>{
      if (err) throw err;
      if (resutls.affectedRows == 1) res.json(1);
      else res.json(0);
    });
    connection.release();
  });
});
//删除分类
router.get('/delete', function (req, res, next) {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query('delete from category where cid=?', req.query.cid, (err, results) => {
      if (err) throw err;
      if (results.affectedRows > 0) {
        res.json(1)
      } else {
        res.json(0);
      }
    });
    connection.release();
  });
});
//修改分类
router.get('/editcate', function (req, res, next) {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    var parms = [req.query.cname, req.query.ccomment,req.query.cid];
    connection.query('update category set cname=?,ccomment=? where cid=?', parms, (err, results) => {
      if (err) throw err;
      if (results.affectedRows > 0) {
        res.json(1)
      } else {
        res.json(0);
      }
    });
    connection.release();
  });
});
//查找分类
router.get('/findcate',function(req,res,next){
  pool.getConnection((err,connection)=>{
    if(err) throw err;
    connection.query('select * from cate where id=?',req.query.id,(err,results)=>{
      if(err) throw err;
      res.json(results);
    });
  });
});
//遍历分类
router.get('/listcate',function(req,res){
  pool.getConnection((err,connection)=>{
    connection.query('select c.cid,c.cname,p.pid,p.pname from category c left outer join profession p on p.parent=c.cid',(err,results)=>{
      if(err) throw err;
      //本类的数组//存类的数组 //类的二级类数组下标 //数组的下表 //保存cid的值
       var lei_arr=[],zuida_arr = [],y = 0, z = 0,temp=1;
      //循环开始
      results.forEach(function(v,i,a){  
        //二级菜单的单条记录对象
        var erji_obj = {'pid':v.pid,'pname':v.pname};
        if(v.cid==temp){
          zuida_arr[z]={'cid':v.cid,'cname':v.cname};
          temp = v.cid;
          if(i!=0){
            y=y+1;
          }
        }else{
          y=0;
          //写完上一次本类的数据
          zuida_arr[z].set=lei_arr;
          //写一次z才加1
          z+=1;
          zuida_arr[z]={'cid':v.cid,'cname':v.cname};
          //清空lei_arr
          lei_arr=[];
        }
        lei_arr[y]=erji_obj;
        //最后一个就输出
        if(i==results.length-1){
          zuida_arr[z].set=lei_arr;
          res.json(zuida_arr);
        }
      });
    });
    connection.release();
  });
});
//查找分类
router.get('/listTopCate',function(req,res,next){
  var page = parseInt(req.query.page);
  var limit = parseInt(req.query.limit);
  pool.getConnection((err, connection) => {
    var count;
    if (err) throw err;
    connection.query('SELECT COUNT(cid) AS COUNT FROM category', (err, results) => {
      if (err) throw err;
      count = results[0].COUNT;
    });
    var sql = 'SELECT * FROM category limit ?,?';
    connection.query(sql, [(page - 1) * limit, limit], (err, results) => {
      if (err) throw err;
      res.json({ "code": 0, "msg": "", "count": count, "data": results });
    });
    connection.release();
  });
});
module.exports = router;
