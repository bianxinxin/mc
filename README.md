# 微课程资源库平台

#### 项目介绍
微课程资源库管理平台后台管理系统。

#### 软件架构
express+mysql+mocha


#### 安装教程

 1. 修改DB/dbconfig.js下的数据库配置文件
 2. node install
 3. node start

#### 使用说明


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
